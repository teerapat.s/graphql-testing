import gql from 'graphql-tag'

export const FLAGMENT_POST = gql`
  fragment PostData on Post {
    id
    title
    body
  }
`
