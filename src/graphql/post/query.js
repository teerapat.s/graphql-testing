import gql from 'graphql-tag'
import { FLAGMENT_POST } from './fragment'

export const GET_POSTS = gql`
  ${FLAGMENT_POST}
  query GetPosts($options: PageQueryOptions!) {
    posts(options: $options) {
      data {
        ...PostData
      }
      meta {
        totalCount
      }
    }
  }
`

export const CREATE_POST = gql`
  mutation ($input: CreatePostInput!) {
    createPost(input: $input) {
      id
      title
      body
    }
  }
`

export const UPDATE_POST = gql`
  mutation ($id: ID!, $input: UpdatePostInput!) {
    updatePost(id: $id, input: $input) {
      id
      title
      body
    }
  }
`

export const DELETE_POST = gql`
  mutation ($id: ID!) {
    deletePost(id: $id)
  }
`

export const GET_POST_BY_USER = gql`
  ${FLAGMENT_POST}
  query ($id: ID!) {
    user(id: $id) {
      posts {
        data {
          ...PostData
        }
      }
    }
  }
`
