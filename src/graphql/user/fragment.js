import gql from 'graphql-tag'

export const FLAGMENT_USER = gql`
  fragment UserData on User {
    id
    name
    username
  }
`
