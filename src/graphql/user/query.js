import gql from 'graphql-tag'
import { FLAGMENT_USER } from './fragment'

export const GET_USERS = gql`
  ${FLAGMENT_USER}
  query GetUsers($options: PageQueryOptions!) {
    users(options: $options) {
      data {
        ...UserData
      }
      meta {
        totalCount
      }
    }
  }
`

export const CREATE_USER = gql`
  mutation ($input: CreateUserInput!) {
    createUser(input: $input) {
      id
      name
      username
    }
  }
`

export const UPDATE_USER = gql`
  mutation ($id: ID!, $input: UpdateUserInput!) {
    updateUser(id: $id, input: $input) {
      id
      name
      username
    }
  }
`

export const DELETE_USER = gql`
  mutation ($id: ID!) {
    deleteUser(id: $id)
  }
`
